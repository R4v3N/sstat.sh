#!/bin/bash
#
# sstat v1.2
# Server Status Tool
#
# By: R4v3N
# http://www.top-hat-sec.com
# admin@top-hat-sec.com
# @TopHatSec
#
# This tool was designed to constantly check the status of a server.
# If you are stress testing or fuzzing a server, this tool will keep
# an eye on the server to see if it gets knocked down and stops responding.

# usage example:
#
# [*] Enter Domain/Web Address: google.com
# [*] How Many Seconds Before Each Check?: 5
#
# The above example will check google.com every 5 seconds.
# email me to report bugs!
# -R4v3N

clear
STD='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'

SESSION=$(date +%m-%d-%y-%H-%M-%S)
mkdir -p $HOME/sstat-logs
rm -rf sstat-logs/*.tmp
f_error(){
	echo "Not A Valid Input.."
	sleep 1
	f_main
}

f_sstatuslog(){
while true
do
	curl -s http://isup.me/$SERVER > $HOME/sstat-logs/sstat.tmp
	VARTIME=$(date +%D::%T)
	if grep -q "looks down from here." $HOME/sstat-logs/sstat.tmp; then
		echo "" >> $HOME/sstat-logs/$SESSION.$SERVER.log
		echo -e "LAST CHECK: " $VARTIME >> $HOME/sstat-logs/$SESSION.$SERVER.log 
                echo -e "SERVER: [" "${CYAN}" $SERVER "]" "${STD}" >> $HOME/sstat-logs/$SESSION.$SERVER.log
                echo -e "IP:     [" "${YELLOW}" $IP "]" "${STD}" >> $HOME/sstat-logs/$SESSION.$SERVER.log
                echo -e "${RED}""SERVER OFFLINE" "${STD}" >> $HOME/sstat-logs/$SESSION.$SERVER.log
# If you do not want audio, comment out the following line.
		# pulseaudio -D &> /dev/null
		mpv offline.wav &> /dev/null
		tail -5 $HOME/sstat-logs/$SESSION.$SERVER.log
                sleep $varsec
        else
		echo "" >> $HOME/sstat-logs/$SESSION.$SERVER.log
		echo -e "LAST CHECK: " $VARTIME >> $HOME/sstat-logs/$SESSION.$SERVER.log
                echo -e "SERVER: [" "${CYAN}" $SERVER "]" "${STD}" >> $HOME/sstat-logs/$SESSION.$SERVER.log
                echo -e "IP:     [" "${YELLOW}" $IP "]" "${STD}" >> $HOME/sstat-logs/$SESSION.$SERVER.log
                echo -e "${GREEN}""SERVER ONLINE" "${STD}" >> $HOME/sstat-logs/$SESSION.$SERVER.log
                tail -5 $HOME/sstat-logs/$SESSION.$SERVER.log
		sleep $varsec
        fi
done
}

f_sstatus(){

while true
do
	curl -s http://isup.me/$SERVER > $HOME/sstat-logs/sstat.tmp
	VARTIME=$(date +%D::%T)
	if grep -q "looks down from here." $HOME/sstat-logs/sstat.tmp; then
		echo ""
                echo -e "LAST CHECK: " $VARTIME
		echo -e "SERVER: [" "${CYAN}" $SERVER "]" "${STD}"
		echo -e "IP:     [" "${YELLOW}" $IP "]" "${STD}"
		echo -e "${RED}""SERVER OFFLINE" "${STD}"
# If you do not want audio, comment out the following line.
		pulseaudio -D &> /dev/null
		# mpv offline.wav &> /dev/null
		sleep $varsec
	else
		echo ""
		echo -e "LAST CHECK: " $VARTIME
                echo -e "SERVER: [" "${CYAN}" $SERVER "]" "${STD}"
                echo -e "IP:     [" "${YELLOW}" $IP "]" "${STD}"
                echo -e "${GREEN}""SERVER ONLINE" "${STD}"
		sleep $varsec
	fi
done
}

f_main(){

echo '''

[ sstat - "Server Status Tool" ]
     Do You Even DDoS Bro?

'''
echo ""
read -p '''[*] Enter Domain/Web Address: ''' varsrv
echo -en "${CYAN}"
echo -e "[" $varsrv "]" "${STD}""SELECTED"
echo ""
read -p '''[*] How Many Seconds Before Each Check?: ''' varsec
echo ""
SERVER=$varsrv
IP=$(nslookup $SERVER | grep "Address:" | sed /"#"/d | awk '{print $2}')
echo ""
read -p '''[*] Would you like to turn on logging?: ''' varlog
if [[ $varlog == 'y' ]] || [[ $varlog == 'Y' ]] || [[ $varlog == 'yes' ]] || [[ $varlog == 'YES' ]]; then
	echo ""
	echo "Ok Logging Enabled.."
	sleep 1
	echo ""
	echo "Logs will be kept in" $HOME/sstat-logs/
	sleep 1
	logans=1
	f_sstatuslog
elif [[ $varlog == 'n' ]] || [[ $varlog == 'N' ]] || [[ $varlog == 'no' ]] || [[ $varlog == 'NO' ]]; then
	echo ""
	echo "Ok, No Logging.."
	logans=0
	sleep 1
	f_sstatus
else
	f_error
fi
}

f_main

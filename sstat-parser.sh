#!/bin/bash
#
# Parse Your Logs!
#
# To properly display your log files from sstat, it may not make sense to use this parser if the logs are small.
# If you have large logs, then you may want to use this tool to help you find what you are looking for.
#

STD='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'


rm -rf $HOME/sstat-logs/parser.tmp
rm -rf $HOME/sstat-logs/parser-log.tmp

f_clean(){
rm -rf $HOME/sstat-logs/parser.tmp
rm -rf $HOME/sstat-logs/parser-log.tmp
f_main

}

f_menu(){
echo ""
echo "PARSING: ""--->" "[" $varlog "]"
SERVER=$(cat $varlog | grep "SERVER\:" | awk '{print $4}' | sort -u | uniq -u)
echo "SERVER:  ""---> ""[" $SERVER "]"
varoff=$(cat $varlog | grep -B 5 "OFFLINE")


if [[ ! -z "$varoff" ]]; then
	echo -e "${RED}""[OFFLINE INSTANCES FOUND]""${STD}"
	echo "------------------------------------------------"
	cat $varlog | grep -B 5 "OFFLINE"
	echo "------------------------------------------------"
	echo "SERVER REPORTED ONLINE AT:"
	echo "------------------------------------------------"
	cat $varlog | grep -B 5 "ONLINE" | grep "LAST CHECK" | awk '{print $3}'| sort -u | uniq -u | while read line; do
                echo $line
        	echo "------------------------------------------------"
	done
else
	echo ""
	echo -e "${YELLOW}""[NO OFFLINE INSTANCES FOUND]""${STD}"
	echo "------------------------------------------------"
	echo "SERVER REPORTED ONLINE AT: "
	echo "------------------------------------------------"
	cat $varlog | grep -B 5 "ONLINE" | grep "LAST CHECK" | awk '{print $3}'| sort -u | uniq -u | while read line; do
		echo $line
		echo "------------------------------------------------"
	done
fi
varip=$(cat $varlog | grep "IP" | awk '{print $4}'| sort -u | uniq -u)
if [[ -z "$varip" ]]; then
	echo -e "${YELLOW}""[NO IPs FOUND]""${STD}"
else
	echo -e "${YELLOW}""[IPs FOUND]""${STD}"
	echo $varip
	echo "------------------------------------------------"
fi
read -p '''[*] Parsing Done.. Press Enter To Continue ''' NULLD
f_clean
}

f_main(){
	clear
	read -p '''

[  SSTAT LOG PARSER TOOL  ]

[1] Remove all logs and temp files.
[2] Parse Logs

[*] Select Option: ''' menopt

	if [ $menopt = 1 ]; then
		echo ""
		echo "##### [!WARNING!] ##### [!WARNING!] ##### [!WARNING! #####" 
		echo "# This will delete all tmp and log files in sstat-logs . #"
		echo "##########################################################"
		echo ""
		echo "[ Press CTRL C To Cancel Now ]"
		echo ""
		sleep 5
		rm -rf $HOME/sstat-logs/*.tmp
		rm -rf $HOME/sstat-logs/*.log
		echo "Files removed..."
		sleep 1
		f_clean
	elif [ $menopt = 2 ]; then
		ls $HOME/sstat-logs/ | grep ".log" > $HOME/sstat-logs/parser.tmp
		logfiles=$(wc -l $HOME/sstat-logs/parser.tmp | awk '{print $1}')
		clear
		echo ""
		echo "Searching For Log Files..."
		sleep 1
		if [ $logfiles = "0" ]; then
			echo ""
			echo "I cant find any logs!"
			sleep 1
			f_clean
		else
			echo ""
			echo "I found some logs!"
			sleep 1
			echo ""
			varlogs=0
			cat $HOME/sstat-logs/parser.tmp | while read line; do
				varlogs=$((varlogs + 1))
				echo "["$varlogs"]" $line >> $HOME/sstat-logs/parser-log.tmp
			done
			cat $HOME/sstat-logs/parser-log.tmp
			echo ""
			read -p '''[*] Select Log Number: ''' lognum
			cd $HOME/sstat-logs/
			line=$(sed -n "${lognum}p" "parser-log.tmp")
			echo ""
			varlog=$(echo $line | awk '{print $2}')
			echo "[SELECTED] -->" $varlog 
			echo ""
			read -p '''[*] Is this correct?: ''' ans
			if [[ $ans == 'y' ]] || [[ $ans == 'Y' ]] || [[ $ans == 'yes' ]] || [[ $ans == 'YES' ]]; then
				f_menu
			else
				f_clean
			fi
		
		fi
	fi
}
f_main
